interface PrivateRouterProps {
  component: React.FC;
  path: string;
  exact: boolean;
  location?: string;
}

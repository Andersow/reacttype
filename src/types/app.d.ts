interface IAppInformation {
  name: string;
  description?: string;
  active: boolean;
}

type AppState = {
  appInformation: IAppInformation;
};

type AppAction = {
  type: string;
  appInformation: IAppInformation;
};

type DispatchTypeApp = (args: AppAction) => AppAction;

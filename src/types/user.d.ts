interface ICredentials {
  name: string;
  email: string;
  isAutenticated?: boolean;
  token?: string;
}

type UserState = {
  credentials: ICredentials;
};

type UserAction = {
  type: string;
  credentials: ICredentials;
};

type DispatchTypeUser = (args: UserAction) => UserAction;

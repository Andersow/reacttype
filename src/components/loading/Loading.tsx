import React from "react";
import "./loading.css";
import { Spin, Alert } from "antd";

interface LoadingProps {
  msgLoaging: string;
}

const Loading: React.VFC<LoadingProps> = ({ msgLoaging }: LoadingProps) => {
  return (
    <Spin tip="Carregando...">
      <Alert message={msgLoaging} description="Further details about the context of this alert." type="info" />
    </Spin>
  );
};
export default Loading;

import { useEffect } from "react";
import { useStore } from "react-redux";
import { RootState } from "../store";
import { loadApp } from "../store/app";
import { login } from "../store/user";
import { useDispatch } from "react-redux";
import type { AppDispatch } from "../store/index";

/**
 * * Hook responsável por recuperar o local storege e gravar no redux;
 */
export const useStateOnRefresh = (credentials: ICredentials): void => {
  const useAppDispatch = (): AppDispatch => useDispatch<AppDispatch>();

  const store = useStore();
  const appDispatch = useAppDispatch();

  if (!credentials.isAutenticated) {
    const reduxStore: RootState = JSON.parse(sessionStorage.getItem("redux-store") || "{}");

    const appState: AppState = reduxStore.app;

    if (appState) {
      const app: IAppInformation = appState.appInformation;

      appDispatch(
        loadApp({
          name: app.name,
          description: app.description,
          active: app.active,
        })
      );
    }

    const userState: UserState = reduxStore.user;
    if (userState) {
      const user: ICredentials = userState.credentials;
      appDispatch(
        login({
          name: user.name,
          email: user.email,
          isAutenticated: user.isAutenticated,
          token: user.token,
        })
      );
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const saveStateOnSessionStorage = (e: any) => {
    sessionStorage.setItem("redux-store", JSON.stringify(store.getState()));
    delete e["returnValue"];
  };

  useEffect(() => {
    window.addEventListener("beforeunload", saveStateOnSessionStorage);

    return () => window.removeEventListener("beforeunload", saveStateOnSessionStorage);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
};

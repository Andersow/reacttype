import { useStore } from "react-redux";

const useLogState = (debug: boolean): void => {
  const store = useStore();
  if (!debug) return;

  store.subscribe(() => {
    console.log("@@@@@@@@  @@@@@@@@  @@@@@@@@  @@@@@@@@  @@@@@@@@");
    console.log("@@           @@     @@    @@  @@    @@  @@      ");
    console.log("@@@@@@@@     @@     @@    @@  @@@@@@@@  @@@@    ");
    console.log("      @@     @@     @@    @@  @@   @@   @@      ");
    console.log("@@@@@@@@     @@     @@@@@@@@  @@    @@  @@@@@@@@");

    console.log(store.getState());
  });
};

export default useLogState;

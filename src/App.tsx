import React from "react";
import ExternalRouters from "./router/ExternalRouters";
import "./App.css";

const App: React.VFC = () => {
  return <ExternalRouters />;
};

export default App;

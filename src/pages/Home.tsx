import React, { useEffect } from "react";
import Page from "../templates/Page";
import type { RootState } from "./../store/index";
import { TypedUseSelectorHook, useSelector } from "react-redux";

const Home: React.VFC = () => {
  const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
  
  const app = useAppSelector((state) => state.app);
  const user = useAppSelector((state) => state.user);

  return (
    <Page title={"Home"}>
      {JSON.stringify(app)}
      <hr />
      {JSON.stringify(user)}
    </Page>
  );
};

export default Home;

import React from "react";

import { Link } from "react-router-dom";

const Welcome: React.FC = () => {
  return (
    <div>
      <ul>
        <li>
          <Link to="/pages/home">Home Page</Link>
        </li>
        <li>
          <Link to="/pages/login">Login</Link>
        </li>
        <li>
          <Link to="/contrato/consultar/vigencia">Consultar Vigência</Link>
        </li>
      </ul>
    </div>
  );
};
export default Welcome;

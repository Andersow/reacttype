import React from "react";
import { Link } from "react-router-dom";
import { Result } from "antd";

const NoMatch: React.FC = () => {
  return (
    <Result
      status="404"
      title="404"
      subTitle="A página que você está tentando acessar não existe."
      extra={
        <Link className="btn btn-primary text-white" to="/">
          Cancelar
        </Link>
      }
    />
  );
};

export default NoMatch;

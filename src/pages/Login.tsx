import React from "react";
import { Form, Input, Button } from "antd";
import { useHistory } from "react-router-dom";
import { loadApp } from "../store/app";
import { login } from "../store/user";
import { useDispatch } from "react-redux";
import type { AppDispatch } from "../store/index";

import "./login.css";

const Login: React.VFC = () => {
  const history = useHistory();
  const appDispatch = useDispatch<AppDispatch>();

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const autenticar = (values: never) => {
    appDispatch(
      loadApp({
        name: "Veneza",
        description: "Sistema Veneza",
        active: true,
      })
    );

    appDispatch(
      login({
        name: "Nykel Andersow",
        email: "nykelandersow@gmail.com",
        isAutenticated: true,
        token: "464s65f4s6df4sa6f54as6dfsdfdsf4ds",
      })
    );

    // appDispatch(login(credentials));

    // const { name, age }: { name: string; age: number } = body.value
    /*     const { email } = values;

    const sessionStorage = window.sessionStorage;
    sessionStorage.clear();

    const credentials: ICredentials = {
      name: "",
      email: email,
    };

    const userAction: UserAction = loginUser(credentials);
    dispatch(userAction);

    */
    history.push("/pages/home");
  };

  return (
    <div className="inner-content-login">
      <Form onFinish={autenticar}>
        <Form.Item label="Login:" name="email" rules={[{ required: true, message: "Informe o login!" }]}>
          <Input allowClear />
        </Form.Item>

        <Form.Item
          label="Senha"
          name="password"
          rules={[
            {
              required: true,
              message: "Informe a senha!",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            Autenticar
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default Login;

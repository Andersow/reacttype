import React from "react";
import { Link } from "react-router-dom";
import { Result } from "antd";

const AccessDenied: React.VFC = () => {
  return (
    <Result
      status="403"
      title="403"
      subTitle="Você não está autorizado a acessar esta página."
      extra={
        <Link className="btn btn-primary text-white" to="/signIn">
          Ir para o Login
        </Link>
      }
    />
  );
};

export default AccessDenied;

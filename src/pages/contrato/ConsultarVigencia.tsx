import React, { useState, useEffect } from "react";
import { Table } from "antd";
import Page from "../../templates/Page";
import Axios from "axios";

interface DataType {
  key: React.Key;
  nomePrefeitura: string;
  numeroContrato: number;
  situacao: string;
  dataFimVigencia: string;
}

const columns = [
  {
    title: "Prefeitura",
    dataIndex: "nomePrefeitura",
  },
  {
    title: "Contrato",
    dataIndex: "numeroContrato",
  },
  {
    title: "Situação",
    dataIndex: "situacao",
    // eslint-disable-next-line react/display-name
    render: (text: string) => <a>{text}</a>,
  },
  {
    title: "Fim Vigência",
    dataIndex: "dataFimVigencia",
  } /* ,
    {
      title: "operation",
      dataIndex: "operation",
      render: (_, record) => (
        <Popconfirm title="Deseja excluir?" onConfirm={() => this.handleDelete(record.key)}>
          Delete
        </Popconfirm>
      ),
    }, */,
];

const ConsultarVigencia: React.VFC = () => {
  const [contratos, setContratos] = useState<DataType[]>([] as DataType[]);
  const [isLoading] = useState<boolean>(false);

  useEffect(() => {
    Axios.get("http://sinternos.des.sp.gov.br/samp-api/api/PrefeituraContratos/Vigencias").then((response) => {
      setContratos(response.data.prefeiturasContrato);
    });
  }, []);

  return (
    <Page title="Consultar Vigência">
      <Table pagination={{ position: ["topRight"] }} loading={isLoading} className="tableCar" columns={columns} dataSource={contratos}></Table>
    </Page>
  );
};

export default ConsultarVigencia;

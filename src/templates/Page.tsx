import React, { MouseEvent, useState, ReactNode } from "react";
import { Layout, Menu, Badge, Space, Divider, Avatar, Dropdown, BackTop, Image, Typography } from "antd";
import { useHistory, Link } from "react-router-dom";
import { LogoutOutlined, MenuUnfoldOutlined, MailOutlined,MenuFoldOutlined, UserOutlined, BellOutlined, DotChartOutlined, DeploymentUnitOutlined, ReconciliationOutlined, ScheduleOutlined, HomeOutlined, FileSearchOutlined } from "@ant-design/icons";
import logo from "../logo.svg";
import { logout } from "../store/user";
import { TypedUseSelectorHook, useSelector, useDispatch } from "react-redux";
import type { RootState, AppDispatch } from "./../store/index";

import "./page.css";

const { Header, Content, Footer, Sider } = Layout;
const { Text } = Typography;
const { SubMenu } = Menu;

type ComponentProps = {
  title: string;
  children: ReactNode;
};

const Page: React.VFC<ComponentProps> = ({ title, children }: ComponentProps) => {
  const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;

  const user = useAppSelector((state) => state.user);

  const appDispatch = useDispatch<AppDispatch>();

  const history = useHistory();
  const [collapsed, setCollapsed] = useState<boolean>(false);

  const menu = (
    <Menu theme="light">
      <Menu.Item key="0">
        <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
          1st menu item
        </a>
      </Menu.Item>
      <Menu.Item
        key="7"
        icon={<LogoutOutlined />}
        onClick={(): void => {
          appDispatch(
            logout({
              name: "",
              email: "",
              isAutenticated: false,
              token: "",
            })
          );
          history.push("/");
        }}
      >
        Sair
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item key="3" disabled>
        3rd menu item（disabled）
      </Menu.Item>
    </Menu>
  );
  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Sider trigger={null} theme="light" collapsible collapsed={collapsed} onCollapse={(collapsed: boolean) => setCollapsed(collapsed)}>
        <div className="wrap-logo d-flex flex-row justify-content-center align-items-center">
          <Image className="logo" preview={false} src={logo} alt="Picture of the author" />
        </div>
        <Menu theme="light" defaultSelectedKeys={["1"]} mode="inline">
          <Menu.Item key="1" icon={<HomeOutlined />}>
            <Link to="/pages/home">Home</Link>
          </Menu.Item>
          <Menu.Item key="2" icon={<ScheduleOutlined />}>
            Apontamento
          </Menu.Item>
          <Menu.Item key="3" icon={<ReconciliationOutlined />}>
            Cadastros
          </Menu.Item>
          <SubMenu key="4" icon={<FileSearchOutlined />} title="Consultas">
            <SubMenu key="sub4" title="Contratos">
              <Menu.Item key="7">
                <Link to="/contrato/consultar/vigencia">Vigentes</Link>
              </Menu.Item>
            </SubMenu>
          </SubMenu>

          <Menu.Item key="5" icon={<DotChartOutlined />}>
            Relatórios
          </Menu.Item>
          <Menu.Item key="6" icon={<DeploymentUnitOutlined />}>
            Operações
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout className="site-layout">
        <Header>
          <div className="d-flex flex-row justify-content-between">
            <div>
              {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                className: "trigger",
                onClick: (): void => {
                  setCollapsed(!collapsed);
                  console.log(title);
                },
              })}
            </div>
            <div>
              <Space size="middle" direction="horizontal" align="center" split={<Divider type="vertical" />}>
                <Badge count={5} size="small">
                  <Avatar size="small" icon={<BellOutlined />} />
                </Badge>

                <Dropdown overlay={menu} trigger={["click"]}>
                  <span className="ant-dropdown-link" onClick={(event: MouseEvent) => event.preventDefault()}>
                    <Space>
                      <Avatar size="small" icon={<UserOutlined />} />
                      <Text>{user.credentials.name}</Text>
                    </Space>
                  </span>
                </Dropdown>
              </Space>
            </div>
          </div>

          {/*           {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
              className: "trigger",
              onClick: toggle,
            })}
 */}
        </Header>
        <Content style={{ margin: "16px 16px" }}>
          {/*             <Breadcrumb style={{ margin: "16px 0" }}>
              <Breadcrumb.Item>User</Breadcrumb.Item>
              <Breadcrumb.Item>Bill</Breadcrumb.Item>
            </Breadcrumb> */}
          <div className="content-main shadow-sm">
            <img src={logo} style={{ width: "50px", height: "auto" }} className="App-logo" alt="logo" />
            {children}
          </div>
          <BackTop />
        </Content>
        <Footer style={{ textAlign: "center" }}>Veneza ©2021 Created by Prodesp</Footer>
      </Layout>
    </Layout>
  );
};

export default Page;

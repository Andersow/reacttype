import "";
/* 

type UserInfo = {
  id: number;
  name: string;
  email: string;
};

const testar = (userInfo: UserInfo): void => {
  console.log("teste");
};

const userInfo: UserInfo = {
  id: 1,
  name: "nykel",
  email: "nykel@gmail",
};
 */


/* 
    ! Partial 
    * Constrói um tipo com todas as propriedades de UserInfo definidas como opcionais.
    * Este utilitário retornará um tipo que representa todos os subconjuntos de um determinado tipo.
 */
/* const userInfo2: Partial<UserInfo> = {
  name: "nykel",
  email: "nykel@gmail",
}; */




/* 
    ! Required
    * Constrói um tipo que consiste em todas as propriedades de UserInfo definidas como obrigatórias.
    * O oposto de Partial. 
*/
/* const userInfo3: Required<UserInfo> = {
  id: 1,
  name: "nykel",
};
 */

/* 
    ! Readonly 
    * Constrói um tipo com todas as propriedades de UserInfo definidas como somente leitura, 
    * o que significa que as propriedades do tipo construído não podem ter seu valor alterado.
*/
/* const userInfo4: Readonly<UserInfo> = {
    id: 1,
    name: "nykel",
  };


userInfo4.email = "teste";

testar(userInfo3);
 */
import "";
/*  */

//boolean
/* let isOn: boolean;
isOn = false; */

// string ("",'',``)
/* let msg: string;
msg = "Estou testando";
msg = "Estou testando";
msg = `Estou verificando se está ${isOn}`; */

// number
/* let total: number;
total = 55; */

// Array (um array de strings)
/* let frutas: string[];
frutas: ["Maçã", "Pera", "Uvas"];

let valores: Array<number>; // Com Generic
valores = [1, 2, 3]; */

// Tupla (Array onde sei a quantidade e os tipos)
/* let title: [number, string];
title = [2, "jjj"];
 */
// Enum
/* enum Colors {
  White = "#fff",
  Black = "#000",
}
 */
/* Colors.Black; */
// Enum como constantes
/* enum Directions {
  UP,
  DOWN,
  LEFT,
  RIGHT,
}

Directions.DOWN; */

// Any (qualquer coisa) tsconfig noImplicitAny
/* let coisa;
coisa = 522; */

//void (vazio, sem retorno)
/* const log = (): void => {
  console.log("teste");
};
 */
// null | undefined (usamos para tipos)
/* let teste1 = undefined;
let teste2 = null; */

// never (nunca vai retornar)
/* const error = (): never => {
  throw new Error("erro");
};

let paraObjetos: object;
paraObjetos: {
  nome: "teste";
} */

/* import ""; */
// type inference
// como passei string o typescript infere que é string
/* let mensagem = "Esta é minha mensagem";
 */
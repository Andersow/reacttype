import "";
/* import ""; */

// Type Alias
/* type Uid = number | string | undefined;

type Platform = "Windows" | "Linux" | "Mac"; */

// Union
// (uma variável pode ser de um tipo u de outro)
/* const f = (uid: number | string, item: string): void => {
  console.log(`Esste é meu ${uid}`);
};

const ff = (uid: Uid, item: string): void => {
  console.log(`Esste é meu ${uid}`);
};

f(5, "teste");
f("5", "teste"); */

// Alias
/* const t = (platform: Platform): void => {
  console.log(`Minha plataforma é ${platform}`);
};

t("Linux"); */
import React from "react";
import { Route, Redirect } from "react-router-dom";
import { TypedUseSelectorHook, useSelector } from "react-redux";
import type { RootState } from "./../store/index";
import { useStateOnRefresh } from "../.../../hooks/useStateOnRefresh";

const PrivateRoute: React.VFC<PrivateRouterProps> = (props: PrivateRouterProps) => {
  const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
  const user = useAppSelector((state) => state.user);
  
  useStateOnRefresh(user.credentials);

  return user.credentials.isAutenticated ? <Route path={props.path} exact={props.exact} component={props.component} /> : <Redirect to={{ pathname: "/acessoNegado", state: { from: props.location } }} />;
};

export default PrivateRoute;

import React from "react";

import { BrowserRouter, Switch, Route } from "react-router-dom";

import Welcome from "../pages/Welcome";
import NoMatch from "../pages/NoMatch";
import Login from "../pages/Login";
import Home from "../pages/Home";
import PrivateRoute from "./PrivateRoute";
import AccessDenied from "../pages/AccessDenied";
import ConsultarVigencia from "../pages/contrato/ConsultarVigencia";

const ExternalRouters: React.VFC = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/">
          <Welcome />
        </Route>

        <Route exact path="/pages/welcome">
          <Welcome />
        </Route>

        <Route exact path="/pages/login">
          <Login />
        </Route>

        <Route exact path="/acessoNegado">
          <AccessDenied />
        </Route>

        <PrivateRoute exact path="/pages/home" component={Home} />
        <PrivateRoute exact path="/contrato/consultar/vigencia" component={ConsultarVigencia} />

        <Route path="*">
          <NoMatch />
        </Route>
      </Switch>
    </BrowserRouter>
  );
};

export default ExternalRouters;

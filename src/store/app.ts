/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { createSlice, PayloadAction } from "@reduxjs/toolkit";

const initialState: AppState = {
  appInformation: {
    name: "",
    description: "",
    active: false,
  },
};

// Slice
const appSlice = createSlice({
  name: "app",
  initialState: {
    appInformation: initialState.appInformation,
  },
  reducers: {
    load: (state, { payload }: PayloadAction<IAppInformation, string>) => {
      state.appInformation = payload;
    },
  },
});

export default appSlice.reducer;

// Actions
const { load } = appSlice.actions;

export const loadApp = (appInformation: IAppInformation) => async (dispatch: (arg0: { payload: IAppInformation; type: string }) => any) => {
  try {
    return dispatch(load(appInformation));
  } catch (e) {
    return console.error(e.message);
  }
};

import { configureStore } from "@reduxjs/toolkit";
import app from "./app";
import user from "./user";

const store = configureStore({
  reducer: {
    app,
    user,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;

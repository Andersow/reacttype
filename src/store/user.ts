/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { createSlice, PayloadAction } from "@reduxjs/toolkit";

const initialState: UserState = {
  credentials: {
    name: "",
    email: "",
    isAutenticated: false,
    token: "",
  },
};

// Slice
const slice = createSlice({
  name: "user",
  initialState: {
    credentials: initialState.credentials,
  },
  reducers: {
    loginApp: (state, { payload }: PayloadAction<ICredentials, string>) => {
      state.credentials = payload;
    },
    logoutApp: (state, { payload }: PayloadAction<ICredentials, string>) => {
      state.credentials = payload;
    },
  },
});

export default slice.reducer;

// Actions
const { loginApp, logoutApp } = slice.actions;

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const login = (credentials: ICredentials) => async (dispatch: (arg0: { payload: ICredentials; type: string }) => any) => {
  try {
    return dispatch(loginApp(credentials));
  } catch (e) {
    return console.error(e.message);
  }
};

export const logout = (credentials: ICredentials) => {

  return async (dispatch: (arg0: { payload: ICredentials; type: string }) => any) => {
    try {
      return dispatch(logoutApp(credentials));
    } catch (e) {
      return console.error(e.message);
    }
  };
};

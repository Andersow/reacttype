import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { ErrorBoundary } from "react-error-boundary";
import ErrorFallback from "./components/errors/ErrorFallback";
import { Provider } from "react-redux";
import store from "./store/index";
import "bootstrap/dist/css/bootstrap.min.css";
import "antd/dist/antd.css";

/* function handleChange() {
  console.log("@@@@@@@@  @@@@@@@@  @@@@@@@@  @@@@@@@@  @@@@@@@@");
  console.log("@@           @@     @@    @@  @@    @@  @@      ");
  console.log("@@@@@@@@     @@     @@    @@  @@@@@@@@  @@@@    ");
  console.log("      @@     @@     @@    @@  @@   @@   @@      ");
  console.log("@@@@@@@@     @@     @@@@@@@@  @@    @@  @@@@@@@@");

  console.log(store.getState());
} */

// eslint-disable-next-line @typescript-eslint/no-unused-vars
// const unsubscribe = store.subscribe(handleChange);

//unsubscribe();

ReactDOM.render(
  <ErrorBoundary FallbackComponent={ErrorFallback}>
    <Provider store={store}>
      {/* <React.StrictMode> */}
      <App />
      {/* </React.StrictMode> */}
    </Provider>
  </ErrorBoundary>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
